//
//  Quote.swift
//  The Swoly Bible
//
//  Created by Pinakpani Mukherjee on 7/15/20.
//  Copyright © 2020 Pinakpani Mukherjee. All rights reserved.
//

import Foundation

import SwiftUI

struct Quote : Hashable, Decodable{
    var quote : String
    var name : String 
}
