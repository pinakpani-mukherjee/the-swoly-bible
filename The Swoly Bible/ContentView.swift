//
//  ContentView.swift
//  The Swoly Bible
//
//  Created by Pinakpani Mukherjee on 7/14/20.
//  Copyright © 2020 Pinakpani Mukherjee. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    var quotes:[Quote]
    
    var body: some View {
        VStack{
            CircleImage2(imageName: "main_image")
                .frame(width:200,height: 160)
                .padding(.top,90)
                .padding(.bottom,20)
            
            MainView(quotes: quotes)
            Spacer()
        }.background(Image("background")
            .resizable()
            .scaledToFill()
            .edgesIgnoringSafeArea(.all))
        
        
        
    }
}

struct MainView: View {
    
    var quotes: [Quote]
    
    var body: some View {
        VStack{
            HStack{
                Text("Words of the Allspotter")
                    .font(.subheadline)
                    .italic()
                    .foregroundColor(Color.white)
            }
            
            ScrollView(.horizontal,showsIndicators: false){
                HStack{
                    ForEach(self.quotes, id: \.name){
                        quote in
                        VStack{
                            CircleImage(imageName: "iron-plates")
                            Text(quote.quote)
                            Divider()
                            Text("By- \(quote.name)")
                            .italic()
                                .font(.custom("Helvetica neue", size: 14))
                        }
                        .frame(width:300,height: 300)
                        .foregroundColor(.gray)
                        .padding(.all,4)
                        .background(Color.white)
                    .cornerRadius(20)
                        .overlay(Rectangle().fill(LinearGradient(gradient: Gradient(colors:[.clear,.gray]), startPoint: .center, endPoint: .topLeading))
                        .clipped()
                        .shadow(radius: 8))
                    }
                    
                    
                }
            }
        }
    }
    
}

struct CircleImage:View {
    var imageName: String
    var body: some View{
        
        Image(imageName)
            .resizable()
            .frame(width:100, height: 100)
            .clipShape(Circle())
            .overlay(Circle()
                .stroke(Color.gray,lineWidth: 2))
            .shadow(radius: 10
        )
        
        
        
    }
}

struct CircleImage2:View {
    var imageName: String
    var body: some View{
        
        Image(imageName)
            .resizable()
            .frame(width:200, height: 200)
            .clipShape(Circle())
            .overlay(Circle()
                .stroke(Color.gray,lineWidth: 2))
            .shadow(radius: 10
        )
        
        
        
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(quotes: quoteData)
    }
}
